package com.example.JavaRiga13Ex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaRiga13ExApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaRiga13ExApplication.class, args);
	}

}
