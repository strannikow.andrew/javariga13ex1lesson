package com.example.JavaRiga13Ex.ex1;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
//TASK 1
//Create a project based on Spring Boot using spring-boot-starter-web starter
// and lombok library. Then create a class DummyLogger in the project,
// which will log at the start of the application with the informationHello from task1.

@Component("dummyLoggerEx1" )  //@Component used for spring to know than need use this Class
@Slf4j // lombok annotation
public class DummyLogger  implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        log.info("Hello from task1");

    }
}
