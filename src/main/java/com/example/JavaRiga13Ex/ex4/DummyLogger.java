package com.example.JavaRiga13Ex.ex4;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DummyLogger {

    public void sayHi(int result, String sentence) {
        log.info(result+" " + sentence);
    }
}