package com.example.JavaRiga13Ex.ex4;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomerConfiguration {
    @Bean("bestDummyLogger")
    public DummyLogger generateBeanDummyLogger(){
        return new DummyLogger();
    }
    @Bean
    public ListUtil listUtility(){
        return new ListUtil();
    }
    @Bean(name  = "stringUtility")
    public StringUtil stringUtil(){
        return new StringUtil();
    }
}
