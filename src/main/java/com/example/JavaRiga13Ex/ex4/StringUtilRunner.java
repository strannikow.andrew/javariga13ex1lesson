package com.example.JavaRiga13Ex.ex4;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StringUtilRunner implements CommandLineRunner {

    private final ListUtil listUtil;
    private final StringUtil stringUtil;
    private final DummyLogger dummyLogger;



    @Override
    public void run(String... args) throws Exception {
         int result = listUtil.sumElements(List.of(2,2));
        String sentence =  stringUtil.formSentence(List.of("best", "advice","ever"));
        dummyLogger.sayHi(result,sentence);
    }
}
