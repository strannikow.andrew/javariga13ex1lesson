package com.example.JavaRiga13Ex.ex3;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DummyLoggerSecondary implements DummyLogger{
    @Override
    public void sayHello() {
      log.info("Secondary logger! ");
    }
}
