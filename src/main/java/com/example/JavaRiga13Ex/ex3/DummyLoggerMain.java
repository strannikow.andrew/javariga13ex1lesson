package com.example.JavaRiga13Ex.ex3;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
@Primary
@Component
@Slf4j
public class DummyLoggerMain implements DummyLogger {
    @Override
    public void sayHello() {
        log.info("Main logger!");

    }
}
