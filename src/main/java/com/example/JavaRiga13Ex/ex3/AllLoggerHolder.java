package com.example.JavaRiga13Ex.ex3;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AllLoggerHolder implements CommandLineRunner {

     private final List<DummyLogger> allDummyLoggers;

    @Override
    public void run(String... args) throws Exception {
        allDummyLoggers.stream().forEach(DummyLogger::sayHello);
    }
}
